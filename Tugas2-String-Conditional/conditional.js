//if-else

var nama = "John"
var peran = ""
var ucapan = "Selamat datang di Dunia Werewolf, " + nama

if(nama == '' && peran == '' ){
    console.log("Nama harus diisi!")
    }
else if(peran == 'penyihir'){
    console.log(ucapan)
    console.log("Halo Penyihir "+ nama +", kamu dapat melihat siapa yang menjadi werewolf!")
    }
else if(peran == 'guard'){
    console.log(ucapan)
    console.log("Halo Guard "+ nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }
else if(peran == 'Werewolf'){
    console.log(ucapan)
    console.log("Halo Werewolf "+ nama +", Kamu akan memakan mangsa setiap malam!")
}else{console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")}

//switch case

var tanggal = 5; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 9; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1998; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch(bulan){
    case 1 : {bulan = "Januari";break;}
    case 2 : {bulan = "Februari";break;}
    case 3 : {bulan = "Maret";break;}
    case 4 : {bulan = "April";break;}
    case 5 : {bulan = "Mei";break;}
    case 6 : {bulan = "Juni";break;}
    case 7 : {bulan = "Juli";break;}
    case 8 : {bulan = "Agustus";break;}
    case 9 : {bulan = "September";break;}
    case 10 : {bulan = "Oktober";break;}
    case 11 : {bulan = "November";break;}
    case 12 : {bulan = "Desember";break;}
}

console.log (tanggal + ' ' + bulan + ' ' + tahun)